# How to read data using Kafka CLI

1. Use or create a Service Account, since individual access to the Kafka cluster is avoided. Then ask the Monit team to give access rights to that account for the specific topic you want.

1. Create a client `jaas.conf` file:

        KafkaClient {
        com.sun.security.auth.module.Krb5LoginModule required
        useTicketCache=true;
        };

1. Create a `consumer.properties` file:

        security.protocol=SASL_SSL
        sasl.mechanism=GSSAPI
        sasl.kerberos.service.name=kafka

1. Export the needed Kafka options:

        $export KAFKA_OPTS='-Djava.security.auth.login.config=<path_to_client_JAAS_file>'

1. Kinit using your Service Account.

1. Execute the command `host monit-kafka.cern.ch` from which you'll get the list of addresses.

1. Then choose one of the addresses and execute `host xxx.xxx.xx.xxx`. You'll get something like this:
`monit-kafkay-xxxxxxxxx.cern.ch.`

1. Last step is to start the console consumer with all the security configurations:

        $KAFKA_FOLDER/bin/kafka-console-consumer.sh --bootstrap-server monit-kafkay-xxxxxxxxx.cern.ch:9093 --topic topic_name --consumer.config <path_to_console_properties>
