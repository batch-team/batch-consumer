name := "kafka-read"
scalaVersion := "2.12.10"
version := "1.0"

libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.2"
libraryDependencies += "org.apache.spark" % "spark-sql-kafka-0-10_2.12" % "3.1.2"
libraryDependencies += "org.apache.spark" % "spark-core_2.12" % "3.1.2"
libraryDependencies += "org.apache.spark" % "spark-sql_2.12" % "3.1.2"

