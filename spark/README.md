# Spark consuming batch_logs from Kafka

## Build

```
sbt package
```

## Run

`./run.sh` or:


```
/root/spark-3.1.2-bin-hadoop3.2/bin/spark-submit \
--driver-java-options "-Djava.security.auth.login.config=./jaas.conf" \
--conf spark.executor.extraJavaOptions=-Djava.security.auth.login.config=./jaas.conf \
--conf spark.kafka.clusters.monit.sasl.token.mechanism=GSSAPI \
--conf spark.kafka.clusters.monit.sasl.kerberos.service.name=kafka \
--conf spark.kafka.clusters.monit.security.protocol=SASL_PLAINTEXT \
--packages org.apache.spark:spark-sql-kafka-0-10_2.12:3.1.2 \
--class "ReadFromKafka" \
--master local[4] \
target/scala-2.12/hello-world_2.12-1.0.jar
```

## How to generate a Keystore and TrustStore

A puppet machine provides by default a Grid Host Certificate.

* ### In order to create a KeyStore you need:

    1. The key that you can find in: `/var/lib/puppet/ssl/private_keys` in PEM format.
    1. The certificate in: `/var/lib/puppet/ssl/certs, also` in PEM format

Then perform the following command:

```
cat /var/lib/puppet/ssl/private_keys/mykey.pem /var/lib/puppet/ssl/certs/mycert.pem>mykeycertificate.pem
```

A text file must be created, that willh contain the key followed by the certificate:

```
openssl pkcs12 -export -in mykeycertificate.pem.txt -out mykeystore.pkcs12 -name myAlias -noiter -nomaciter
```

This command prompts the user for a password. The password is mandatory. This command also uses the openssl pkcs12 command to generate a PKCS12 KeyStore with the private key and certificate. The generate
d KeyStore is `mykeystore.pkcs12` with an entry specified by the `myAlias` alias. This entry contains the private key and the certificate provided by the `-in` argument. The `noiter` and `nomaciter` opti
ons must be specified to allow the generated KeyStore to be recognized properly by JSSE.

* ### To create a TrustStore you need the certificates:
    1. `/etc/openldap/cacerts/CERN_Grid_Certification_Authority.pem`

    1. `/etc/openldap/cacerts/CERN_Root_Certification_Authority_2.pem`

You have to add those certificates in the TrustStore:

```
keytool -import -file /etc/openldap/cacerts/CERN_Grid_Certification_Authority.pem -alias CERN_Grid_Certification_Authority -keystore myTrustStore
```

The first entry creates a KeyStore file named `myTrustStore` in the current working directory and imports the `CERN_Grid_Certification_Authority` certificate into the TrustStore with an alias of `CERN_Grid_Certification_Authority`. The format of `myTrustStore` is JKS.

For the second entry, subtitute `CERN_Root_Certification_Authority_2` to import the `CERN_Root_Certification_Authority_2` certificate in the TrustStore `myTrustStore`:

```
keytool -import -file /etc/openldap/cacerts/CERN_Root_Certification_Authority_2.pem -alias CERN_Root_Certification_Authority_2 -keystore batchmon.truststore
```

Once completed, myTrustStore is available to be used as the TrustStore for the adapter.
