package consumer
//package sys

import org.apache.spark.sql.SparkSession
// import org.apache.spark.sql.types.{IntegerType, StringType, StructType}

object ReadFromKafka {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession
        .builder
        .appName("Simple Application")
        .config("spark.executor.extraJavaOptions", "java.security.auth.login.config=jaas.conf")
        .getOrCreate()

    val df = spark
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "monit-kafkay-139aed5efb.cern.ch:9093,monit-kafkay-c0808ac86b.cern.ch:9093")
      .option("kafka.sasl.mechanism", "GSSAPI")
      .option("kafka.sasl.kerberos.service.name", "kafka")
      .option("kafka.security.protocol", "SASL_SSL")
      .option("subscribe", "batch_logs")
      .load()

    val query = df.selectExpr("CAST(value AS STRING)")
    val q = query.writeStream
      .format("console")
      .start()
      .awaitTermination()

    //val schema = new StructType()
    //     .add("agent.name", StringType)

    //val msg = df.selectExpr("CAST(value AS STRING)")
    //          .select(from_json(col("value"), schema).as("data"))
    //          .select("data.*")

    //val q = msg.writeStream
    //  .format("console")
    //  .start()
    //  .awaitTermination()
  }
}

