/root/spark-3.1.2-bin-hadoop3.2/bin/spark-submit \
	--driver-java-options "-Djava.security.auth.login.config=./jaas.conf" \
	--conf spark.executor.extraJavaOptions=-Djava.security.auth.login.config=./jaas.conf \
	--conf spark.kafka.clusters.monit.sasl.token.mechanism=GSSAPI \
	--conf spark.kafka.clusters.monit.sasl.kerberos.service.name=kafka \
	--conf spark.kafka.clusters.monit.security.protocol=SASL_SSL \
	--conf spark.kafka.clusters.monit.keystore.location=/root/keys_and_CAs/batchmon-keystore.pkcs12 \
	--conf spark.kafka.clusters.monit.keystore.password=/root/keys_and_CAs/keystore.pass \
	--conf spark.kafka.clusters.monit.keystore.location=/root/keys_and_CAs/batchmon.truststore \
	--conf spark.kafka.clusters.monit.keystore.password=/root/keys_and_CAs/truststore.pass \
	--packages org.apache.spark:spark-sql-kafka-0-10_2.12:3.1.2 \
	--class "consumer.ReadFromKafka" \
	--master local[4] \
	target/scala-2.12/kafka-read_2.12-1.0.jar

